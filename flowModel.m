function B = flowModel(t,B,a,NPP,U)


B(1) = a(3,1)*B(3) + a(2,1)*B(2) - NPP + U(t);
B(2) = NPP - (a(2,3) + a(2,1))*B(2);
B(3) = a(2,3)*B(2) - a(3,1)*B(3);