function B = flowModel2(t,B,a,NPP0,beta,B0,CO2Emissions)

% NPP
f = @(x) NPP0 * (1 + beta*log(x/B0));
NPP = f(B(1));
% ---

B(1) = a(3,1)*B(3) + a(2,1)*B(2) - NPP + CO2Emissions(round(t));
B(2) = NPP - (a(2,3) + a(2,1))*B(2);
B(3) = a(2,3)*B(2) - a(3,1)*B(3);