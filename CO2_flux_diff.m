function [ dB ] = CO2_flux_diff(t,B,A,NPP0,beta,B_10)
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here
dB=zeros(3,1);

dB(1)=A(3,1)*B(3)+A(2,1)*B(2)-NPP0*(1+beta*log(B(1)/B_10));
dB(2)=NPP0*(1+beta*log(B(1)/B_10))-(A(2,1)+A(2,3)*B(2));
dB(3)=A(2,3)*B(2)-A(3,1)*B(3);

end

