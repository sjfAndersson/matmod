%% Diskret tid

steps=length(CO2Emissions); % number of years

B=zeros(steps,3); % GtC for each Box
B(1,:)=B0; % Intial values

NPP = @(x) NPP0 * (1 + beta*log(x/B0(1))); % Transfer function between box1 and box2 

% Iteration for disctete timesteps
for i=2:steps
    B(i,1)=B(i-1,1)+a(2,1)*B(i-1,2)+a(3,1)*B(i-1,3)-NPP(B(i-1,1))+CO2Emissions(i-1); %B(i-1,1)+B(i-1,:)*a(:,1)-NPP(B(i-1,1))+CO2Emissions(i-1);
    B(i,2)=B(i-1,2)+NPP(B(i-1,1))-(a(2,1)+a(2,3))*B(i-1,2);
    B(i,3)=B(i-1,3)+a(2,3)*B(i-1,2)-a(3,1)*B(i-1,3);
end

figure(1)
clf
title('GtC for each Box')
hold all
plot(1:steps,B(:,1),'b')
plot(1:steps,B(:,2),'r')
plot(1:steps,B(:,3),'g')
legend('Atmosf�r','Bio ovan mark','Under mark')

figure(2)
clf
title('Concentration of CO2')
hold on
plot(1:steps,B(:,1)*0.469,'b')
plot(1:steps,CO2Conc,'r')
legend('Simulation','Data')






